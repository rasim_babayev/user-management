package com.example.management.service;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import com.example.management.entity.User;
import com.example.management.repository.UserRepository;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    public void save(User user) {
        userRepository.save(user);
    }

    public void update(User user) {
        userRepository.findById(user.getId())
                .map(userRepository::save)
                .orElseThrow(() -> new RuntimeException(String.format("User not found by id = %s", user.getId())));
    }

    public void deleteById(String id) {
        userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("User not found by id = %s", id)));
        userRepository.deleteById(id);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
